def importer_mots():
    with open("dico_sutom.txt") as f:
        liste_mots = f.read().splitlines()

    return [mot.lower() for mot in liste_mots]


def creer_dictionnaire():
    liste_mots = importer_mots()
    dictionnaire = {}

    for mot in liste_mots:
        longueur = len(mot)

        if (longueur in dictionnaire):
            dictionnaire[longueur].append(mot)

        else:
            dictionnaire[longueur] = [mot]

    return dictionnaire


def nombre_occurences_lettre(lettre, mot):
    nb_occurences = 0
    for l in mot:
        if l == lettre:
            nb_occurences += 1

    return nb_occurences


def nettoyer_input(input_user):
    return input_user.trim().lower().replace(" ", "")
